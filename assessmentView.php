<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php include 'includes/styles.php'?>   
    <!--/ styles -->   
   
</head>
<body class="sb-nav-fixed user-screen">
    <?php include 'includes/objectArray.php'?>  
   <?php
    include 'includes/headerPostlogin.php';
   ?>

    <!-- main -->
    <div id="layoutSidenav">
        <?php 
            include 'includes/userAside.php';
        ?>

        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-sm-flex justify-content-between pb-2 border-bottom">
                    <h1 class="mt-2 fbold h3 mb-0 pb-0">Maths <small>Assessment Details</small></h1>
                    <ol class="breadcrumb mb-1 pb-0 pl-0 pl-sm-3">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Assessment View</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">  
                     <!-- container fluid -->
                     <div class="container-fluid">

                        <!--Horizontal Tab-->
                        <div class="parentHorizontalTab pt-5">
                            <ul class="resp-tabs-list hor_1 row">
                                <li class="col-md-3 text-center"><span class="icon-book icomoon"></span> Assessment Details</li>
                                <li class="col-md-3 text-center"><span class="icon-question-circle icomoon"></span> Questions</li>
                                <li class="col-md-3 text-center"><span class="icon-group icomoon"></span> Users</li>
                            </ul>
                            <div class="resp-tabs-container hor_1">
                                <!-- tab 1 view-->
                                <div>                                 
                                    <div class="p-1 pmd-5">

                                     <!-- form group -->
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label"><b>Category</b></label>
                                        <div class="col-sm-8 ">
                                            <p>Category 01</p>
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                      <!-- form group -->
                                      <div class="form-group row">
                                        <label class="col-sm-2 col-form-label"><b>Assessment Title</b></label>
                                        <div class="col-sm-8 ">
                                            <p>Maths</p>
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                    
                                      <!-- form group -->
                                      <div class="form-group row">
                                        <label class="col-sm-2 col-form-label"><b>Due Date</b></label>
                                        <div class="col-sm-8 ">
                                            <p>28-08-2020</p>
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                    <!-- form group -->
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label"><b>Time</b></label>
                                        <div class="col-sm-8 ">
                                            <p>No Time Limit</p>
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                    <!-- form group -->
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label"><b>Allow Repeat</b></label>
                                        <div class="col-sm-8 ">
                                            <p>On</p>
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                     <!-- form group -->
                                     <div class="form-group row">
                                        <label class="col-sm-2 col-form-label"><b>Status</b></label>
                                        <div class="col-sm-8 ">
                                            <p><span class="badge badge-success">Active</span></p>
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                    
                                     <!-- form group -->
                                     <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">&nbsp;</label>
                                        <div class="col-sm-8 ">
                                            <input type="submit" class="btn bluebtn" value="Edit">
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                     
                                    </div>
                                </div>
                                <!-- /tab 1 view-->
                                <div>
                                    <div class="p-1 pmd-5 table-responsive">
                                        <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>S.No:</th>
                                                    <th>Display Order</th>
                                                    <th>Question</th>
                                                    <th>Answer Type</th>
                                                    <th>No.of Options</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                       1
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select class="form-control w-50">
                                                                <option>3</option>
                                                                <option>2</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>How Many Days do we have in a week</td>
                                                    <td>Radio</td>
                                                    <td>2</td>
                                                    <td class="actions-icons">
                                                        <a href="javascript:void(0)"><span class="icon-edit icomoon"></span></a>
                                                        <a href="javascript:void(0)"><span class="icon-trash-o icomoon"></span></a>                                                        
                                                    </td>                       
                                                </tr>
                                                <tr>
                                                    <td>
                                                       1
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select class="form-control w-50">
                                                                <option>3</option>
                                                                <option>2</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>How Many Days do we have in a week</td>
                                                    <td>Radio</td>
                                                    <td>2</td>
                                                    <td class="actions-icons">
                                                        <a href="javascript:void(0)"><span class="icon-edit icomoon"></span></a>
                                                        <a href="javascript:void(0)"><span class="icon-trash-o icomoon"></span></a>                                                        
                                                    </td>                       
                                                </tr>                      
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div>
                                    <div class="p-1 pmd-5">
                                        <ul>
                                            <li>Name of User</li>
                                            <li>Name of User</li>
                                            <li>Name of User</li>
                                            <li>Name of User</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Horizontal Tab-->
                    </div>
                    <!--/ container fluid -->
                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->



<?php include 'includes/scripts.php'?> 
</body>
</html>