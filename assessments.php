<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php include 'includes/styles.php'?>   
    <!--/ styles -->   
   
</head>
<body class="sb-nav-fixed user-screen">
    <?php include 'includes/objectArray.php'?>  
   <?php
    include 'includes/headerPostlogin.php';
   ?>

    <!-- main -->
    <div id="layoutSidenav">
        <?php 
            include 'includes/userAside.php';
        ?>
        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-sm-flex justify-content-between pb-2 border-bottom">
                    <h1 class="mt-2 fbold h3 mb-0 pb-0">Assessments</h1>
                    <ol class="breadcrumb mb-1 pb-0 pl-0 pl-sm-3">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Assesments</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">                 

                    <!-- container fluid -->
                    <div class="container-fluid">
                        <!-- exam title -->
                        <div class="title d-sm-flex justify-content-between py-4">
                            <article>
                                <h2 class="h4 pt-3 fbold">List of Assessement</h2>                                                               
                            </article>
                            <p class="text-right pt-3"><a class="bluebtn" href="createAssessment.php"><span class="icon-plus"></span> Create Assesment</a></p>
                        </div>
                        <!--/ exam title -->
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Filter By</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Filter By</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Filter By</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/ row -->
                       <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap widthTable" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="widthCol">Assessment</th>
                                    <th>Questions</th>
                                    <th>Due Date</th>
                                    <th>Status</th>                                  
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <a href="javascript:void(0)"> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Blanditiis voluptate excepturi molestiae explicabo exercitationem optio delectus magnam esse, libero, totam corrupti quas. Soluta, eligendi non sit eos placeat provident quasi accusamus unde accusantium repellendus dolor animi tempora, </a>
                                    </td>                                   
                                    <td>4</td>
                                    <td>28-08-2020</td>
                                    <td><span class="badge badge-success">Active</span></td>
                                    <td class="actions-icons">
                                        <a href="javascript:void(0)"><span class="icon-edit icomoon"></span></a>
                                        <a href="javascript:void(0)"><span class="icon-trash-o icomoon"></span></a>
                                        <a href="javascript:void(0)"><span class="icon-plus icomoon"></span></a>
                                        <a href="javascript:void(0)"><span class="icon-group icomoon"></span></a>
                                    </td>                       
                                </tr>
                                <tr>
                                    <td>
                                        <a href="javascript:void(0)">Additions</a>
                                    </td>                                 
                                    <td>4</td>
                                    <td>28-08-2020</td>
                                    <td><span class="badge badge-danger">In Active</span></td>
                                    <td class="actions-icons">
                                        <a href="javascript:void(0)"><span class="icon-edit icomoon"></span></a>
                                        <a href="javascript:void(0)"><span class="icon-trash-o icomoon"></span></a>
                                        <a href="javascript:void(0)"><span class="icon-plus icomoon"></span></a>
                                        <a href="javascript:void(0)"><span class="icon-group icomoon"></span></a>
                                    </td>                       
                                </tr>                      
                            </tbody>
                        </table>
                       </div>

                    </div>
                    <!--/ container fluid -->
                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->

<?php include 'includes/scripts.php'?> 
</body>
</html>