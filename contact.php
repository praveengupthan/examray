<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php 
        include 'includes/styles.php'
    ?>   
    <!--/ styles -->   
    <?php 
        include "includes/objectArray.php"
    ?>
</head>
<body>

<?php
    include "includes/header.php"
?>

<!-- main -->
<main class="subPage">
    <!-- header of sub page -->
    <div class="subpageHeader">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row justify-content-center">
                <div class="col-md-6 text-center">
                    <h1>Contact</h1>
                    <p>Reach us</p>
                </div>
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ header of sub page -->

    <!-- main of sub page -->
    <div class="mainPagae">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- left col -->
                <div class="col-md-8">
                    <h2 class="h2 fsbold">Let’s build something great together</h2>
                    <p>Whatever your ambition, we’d love to design and build your next big idea or lend a hand on an existing one.</p>

                    <!-- form -->
                    <form id="contact_form" class="form pt-2" action="" method="post">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-6">
                                <div class="form-group customForm">
                                    <label>Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="name" >
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-6">
                                <div class="form-group customForm">
                                    <label>Phone Number (Optional)</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="phone" >
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-6">
                                <div class="form-group customForm">
                                    <label>Email Address</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="email" >
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->                           
                             
                            <!-- col -->
                            <div class="col-md-6">
                                <div class="form-group customForm">
                                    <label>How did you first hear of us</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="howDid" >
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                        <!-- col -->
                        <div class="col-md-6">
                            <div class="form-group customForm">
                                <label>Subject</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="sub" >
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-md-12">
                            <div class="form-group customForm">
                                <label>Message</label>
                                <div class="input-group">
                                    <textarea class="form-control" name="msg" style="height:100px;"></textarea>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-md-12">                           
                               <button class="btn bluebtn w-100" name="submitContact">Submit</button>                            
                        </div>
                        <!--/ col -->                        
                        </div>
                        <!--/ row -->
                    </form>
                    <!--/ form -->
                </div>
                <!--/ left col -->

                 <!-- col -->
                <div class="col-md-4 rtContact">
                    <div class="address">
                        <table class="table table-borderless">
                            <tr>
                                <td class="fsbold">Address:</td>
                                <td>
                                    <h6><b>EXAMRAY<br> PRIVATE LIMITED</b></h6>
                                    <p class="text-left">Hyderabad 500035, Telangana, India</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="fsbold">Contact:</td>
                                <td>
                                    <p class="pb-0">+91 9642123254</p>
                                    <p>+91 7995165789</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="fsbold">Email:</td>
                                <td>
                                    <p class="pb-0">info@examray.com</p>
                                    <p>contact@examray.com</p>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
                <!--/ col -->
            </div>
            <!--/row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ main of sub page -->
</main>

<!--/ main -->

<?php 
    include "includes/footer.php"
?>

<?php include 'includes/scripts.php'?> 
</body>
</html>