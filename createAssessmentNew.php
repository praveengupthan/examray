<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php include 'includes/styles.php'?>   
    <!--/ styles -->   
   
</head>
<body class="sb-nav-fixed user-screen">
    <?php include 'includes/objectArray.php'?>  
   <?php
    include 'includes/headerPostlogin.php';
   ?>

    <!-- main -->
    <div id="layoutSidenav">
        <?php 
            include 'includes/userAside.php';
        ?>

        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-sm-flex justify-content-between pb-2 border-bottom">
                    <h1 class="mt-2 fbold h3 mb-0 pb-0">New Assessment</h1>
                    <ol class="breadcrumb mb-1 pb-0 pl-0 pl-sm-3">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">New Assessment</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                  

                     <!-- container fluid -->
                     <div class="container-fluid">

                     <!-- form -->
                    <form class="form-horizontal" method="">

                        <!-- form group -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select / Add Category</label>
                            <div class="col-sm-8 ">
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Select Assessment Category</option>
                                        <option>Option</option>
                                        <option>Option</option>
                                        <option>Option</option>
                                        <option>Option</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/ form group -->

                         <!-- form group -->
                         <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Add New Category</label>
                            <div class="col-sm-8 ">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Add New Category"/>
                                </div>
                            </div>
                        </div>
                        <!--/ form group -->

                         <!-- form group -->
                         <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Assessment Title</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="password" class="form-control" placeholder="Assessment Title"/>
                                </div>
                            </div>
                        </div>
                        <!--/ form group -->

                        <!-- form group -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Due Date</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" placeholder="Select Date"/>
                                </div>
                            </div>
                        </div>
                        <!--/ form group -->

                        
                        <!-- form group -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-8">
                                <div class="page-wrapper box-content">
                                    <textarea class="content" name="example"></textarea>
                                </div>
                            </div>
                        </div>
                        <!--/ form group -->

                         
                        <!-- form group -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Time Limit</label>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                                    <label class="form-check-label" for="exampleRadios1">
                                        Set Time Limit
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                                    <label class="form-check-label" for="exampleRadios2">
                                        No Time Limit
                                    </label>
                                </div>       
                            </div>
                        </div>
                        <!--/ form group -->

                         
                        <!-- form group -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Set Time</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="password" class="form-control" placeholder="Please Set the time in format 00:00:00"/>
                                </div>
                            </div>
                        </div>
                        <!--/ form group -->

                         <!-- form group -->
                         <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Allow Repeat</label>
                            <div class="col-sm-8">
                                <div class="form-check">                                       
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1">
                                        Yes
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!--/ form group -->

                         <!-- form group -->
                         <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Assessment Image / Logo</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="file" class="form-control" placeholder="Image"/>
                                </div>
                            </div>
                        </div>
                        <!--/ form group -->

                         <!-- form group -->
                         <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Show Results</label>
                            <div class="col-sm-8">
                                <div class="form-check">                                       
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1">
                                        Yes
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!--/ form group -->

                         <!-- form group -->
                         <div class="form-group row">
                            <label class="col-sm-2 col-form-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="submit" class="btn bluebtn w-100" value="Submit">
                            </div>
                        </div>
                        <!--/ form group -->
                    </form>
                    <!--/ form -->
                    
                     
                           
                       

                    </div>
                    <!--/ container fluid -->


                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->



<?php include 'includes/scripts.php'?> 
</body>
</html>