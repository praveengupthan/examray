<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php include 'includes/styles.php'?>   
    <!--/ styles -->   
   
</head>
<body class="sb-nav-fixed user-screen">
    <?php include 'includes/objectArray.php'?>  
   <?php
    include 'includes/headerPostlogin.php';
   ?>
    <!-- main -->
    <div id="layoutSidenav">
        <?php 
            include 'includes/userAside.php';
        ?>
        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-sm-flex justify-content-between pb-2 border-bottom">
                    <h1 class="mt-2 fbold h4 mb-0 pb-0 pl-0 pl-sm-3">Dashboard</h1>
                    <ol class="breadcrumb mb-1 pb-0">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                    <!-- container fluid -->
                    <div class="container-fluid">

                        <!-- row -->
                        <div class="row dbrow">
                            <!-- col -->
                            <div class="col-md-4">
                                <div class="dbCol">
                                    <h6>You Created</h6>
                                    <div class="d-flex justify-content-between iconrow">
                                        <div><span class="icon-pencil icomoon"></span></div>
                                        <div class="text-right">
                                            <h2>25</h2>
                                            <p>Assessments</p>
                                        </div>
                                    </div>
                                    <p></p>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="dbCol">
                                    <h6>Assigned to you</h6>
                                    <div class="d-flex justify-content-between iconrow">
                                        <div><span class="icon-th-list icomoon"></span></div>
                                        <div class="text-right">
                                            <h2>15</h2>
                                            <p>Assessments</p>
                                        </div>
                                    </div>
                                    <p></p>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="dbCol">
                                    <h6>Exam Ray Practice Tests</h6>
                                    <div class="d-flex justify-content-between iconrow">
                                        <div><span class="icon-book icomoon"></span></div>
                                        <div class="text-right">
                                            <h2>55</h2>
                                            <p>Tests</p>
                                        </div>
                                    </div>
                                    <p></p>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->   
                    </div>
                    <!--/ container fluid -->                 

                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->



<?php include 'includes/scripts.php'?> 
</body>
</html>