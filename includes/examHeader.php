 <!-- header-->
 <header class="test-header">
       <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-4 align-self-center">
                    <a class="navbar-brand" href="index.php">
                        <img src="img/logo.svg" alt="">
                    </a>
                </div>
                <div class="col-md-8 col-12">
                    <div class="d-sm-flex">
                         <a href="javascript:void(0)">
                            <h2 class="h2 fwhite">Edu Test</h2>
                        </a>
                        <p>Azure Fundamentals Certification - 2020</p>
                    </div>
                </div>
            </div>
       </div>
</header>
<!--/ hededer -->