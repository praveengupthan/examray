<!-- footer -->
<footer>
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row justify-content-center">
            <!-- col -->
            <div class="col-md-8 text-center">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Subscriptions</a>
                    </li> 
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact</a>
                    </li> 
                </ul>

                <a class="footer-brand" href="index.php">
                    <img src="img/logo-color.svg" alt="">
                </a>
                <p class="text-center">
                    <small>Copyright 2020. All rights reserved.</small>
                </p>
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</footer>
<!--/ footer -->