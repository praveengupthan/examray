<!-- header -->
<header class="bsnav-sticky bsnav-sticky-slide">  
    <!-- nav bar -->
    <div class="navbar navbar-expand-sm bsnav  container">
        <a class="navbar-brand" href="index.php">
            <img src="img/logo-color.svg" alt="">
        </a>
        <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse justify-content-sm-end">
            <ul class="navbar-nav navbar-mobile mr-0">          
            <li class="nav-item"><a class="nav-link" href="signin.php">Signin</a></li>
            <li class="nav-item"><a class="nav-link" href="signin.php">Subscriptions</a></li>
            <li class="nav-item linkparent"><a class="linkbtn" href="signup.php">Signup</a></li>               
            </ul>
        </div>
    </div>
    <div class="bsnav-mobile">
        <div class="navbar"></div>
    </div>
    <!--/ nav bar -->   
</header>
<!--/ header -->