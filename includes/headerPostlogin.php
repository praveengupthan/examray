 <!-- nav -->
 <nav class="sb-topnav navbar navbar-expand navbar-dark">
        <a class="navbar-brand" href="index.html"><img src="img/logo-color.svg"></a>
        <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
        <!-- Navbar-->
        <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a href="javascript:void(0)" class="bluebtn newuserbtn "><span class="icon-plus"></span> New Assessment</a>  
        </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-user"></i><span class="nameMobile"> Username</span> </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">Settings</a>
                    <a class="dropdown-item" href="#">My Profile</a>
                    <a class="dropdown-item" href="#">Change Password</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="login.html">Logout</a>
                </div>
            </li>
        </ul>
    </nav>
    <!--/ nav -->
