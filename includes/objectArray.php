<?php 

$howItWorks=array(
    array(
        "howimg01",
        "Create a board for any project, give it a name, and invite your team."
    ),
    array(
        "howimg02",
        "Create a board for any project, give it a name, and invite your team."
    ),
    array(
        "howimg03",
        "Create a board for any project, give it a name, and invite your team."
    ),
    array(
        "howimg01",
        "Create a board for any project, give it a name, and invite your team."
    ),
    array(
        "howimg02",
        "Create a board for any project, give it a name, and invite your team."
    ),    
);

$examway=array(
    array(
        "the-teamplaybook",
        "A Productive platform ",
        "one unified way of evaluating learnings, End to End online experience, automated valuations a productive way to achieve greater results in less time while giving very sophisticated experience to examiners and examinees. 
        "
    ),
    array(
        "platform",
        "Truly Digital experience",
        "A truly digital way of assessing the evaluation needs while moving away from legacy practices of submissions thorough photos and attachments. 
        "
    ),
    array(
        "sync",
        "Learn, Practice and Evaluate ",
        "Examray, one platform where you learn, practice and evaluate using our 100’s of free tests "
    ),
);

$clientLogos=array(
    array("1"),
    array("2"),
    array("3"),
    array("4"),
    array("5"),
    array("6"),
    array("1"),
    array("2"),
    array("3"),
    array("4"),
    array("5"),
    array("6"),
);

$examListItem=array(
    array(
        "Assigned",
        "IDBI Asst. Manager - Full Mock Test",
        "IDBI Asst Manatger",
        "June 30, 2020",
        "200",
        "200",
        "120",
        "Free / Paid"
    ),
    array(
        "Assigned",
        "IDBI Asst. Manager - Full Mock Test",
        "IDBI Asst Manatger",
        "June 30, 2020",
        "200",
        "200",
        "120",
        "Free / Paid"
    ),   
    array(
        "Assigned",
        "IDBI Asst. Manager - Full Mock Test",
        "IDBI Asst Manatger",
        "June 30, 2020",
        "200",
        "200",
        "120",
        "Free / Paid"
    ),   
    array(
        "Assigned",
        "IDBI Asst. Manager - Full Mock Test",
        "IDBI Asst Manatger",
        "June 30, 2020",
        "200",
        "200",
        "120",
        "Free / Paid"
    ),   
    array(
        "Assigned",
        "IDBI Asst. Manager - Full Mock Test",
        "IDBI Asst Manatger",
        "June 30, 2020",
        "200",
        "200",
        "120",
        "Free / Paid"
    ),   
    array(
        "Assigned",
        "IDBI Asst. Manager - Full Mock Test",
        "IDBI Asst Manatger",
        "June 30, 2020",
        "200",
        "200",
        "120",
        "Free / Paid"
    ),   
    array(
        "Assigned",
        "IDBI Asst. Manager - Full Mock Test",
        "IDBI Asst Manatger",
        "June 30, 2020",
        "200",
        "200",
        "120",
        "Free / Paid"
    ),   
    array(
        "Assigned",
        "IDBI Asst. Manager - Full Mock Test",
        "IDBI Asst Manatger",
        "June 30, 2020",
        "200",
        "200",
        "120",
        "Free / Paid"
    ),   
    array(
        "Assigned",
        "IDBI Asst. Manager - Full Mock Test",
        "IDBI Asst Manatger",
        "June 30, 2020",
        "200",
        "200",
        "120",
        "Free / Paid"
    ),      
);



$freeExamItem=array(
    array(       
        "IDBI Asst. Manager - Full Mock Test",
        "June 30, 2020",
        "200",
        "200",
        "34 Min",
        "20"
    ),
    array(       
        "IDBI Asst. Manager - Full Mock Test",
        "June 30, 2020",
        "200",
        "200",
        "34 Min",
        "20"
    ),
    array(       
        "IDBI Asst. Manager - Full Mock Test",
        "June 30, 2020",
        "200",
        "200",
        "34 Min",
        "20"
    ),
    array(       
        "IDBI Asst. Manager - Full Mock Test",
        "June 30, 2020",
        "200",
        "200",
        "34 Min",
        "20"
    ),
    array(       
        "IDBI Asst. Manager - Full Mock Test",
        "June 30, 2020",
        "200",
        "200",
        "34 Min",
        "20"
    ),
    array(       
        "IDBI Asst. Manager - Full Mock Test",
        "June 30, 2020",
        "200",
        "200",
        "34 Min",
        "20"
    ),
    
     
);


$reviewItem=array(
    array(       
        "Praveen Kumar",
        "June 30, 2020",
        "200",
        "150",
        "50",
        "Submitted"
    ),

    array(       
        "Rajesh Kumar",
        "June 02, 2020",
        "100",
        "75",
        "25",
        "Pending"
    ),

    array(       
        "Rajesh Kumar",
        "June 02, 2020",
        "100",
        "75",
        "25",
        "Pending"
    ),

    array(       
        "Rajesh Kumar",
        "June 02, 2020",
        "100",
        "75",
        "25",
        "Pending"
    ),
    
    
     
);

?>