 <!-- script -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/swiper.min.js"></script>
<script src="js/jquery.bootstrap.wizard.min.js"></script>
<script src="js/accordion.js"></script>
<script src="js/bsnav.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="js/custom.js"></script>
<!--/ script -->

<!-- scripts admin -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
<!--/ scripts admin -->

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/dataTables.responsive.min.js"></script>    
<script src="js/responsive.bootstrap4.min.js"></script>

<script>
    // $(document).ready(function() {
    // var table = $('#example').removeAttr('width').DataTable( {       
    //     scrollX:        true,
    //     scrollCollapse: true,
    //     paging:         false,
    //     columnDefs: [
    //         { width: 200, targets: 0 }
    //     ],
    //     fixedColumns: true
    // } );

     $(document).ready(function() {
         $('#example').DataTable();
    } );
} );
</script>
<script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
<script>
    $('.datepicker').datepicker({
        uiLibrary: 'bootstrap4'
    });
</script>
<script src="js/jquery.richtext.js"></script>
<script>
    $(document).ready(function() {
        $('.content').richText();
    });
</script>
<script src="js/easyResponsiveTabs.js"></script>