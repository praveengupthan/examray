<!-- left nav -->
<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">                       
                <a class="nav-link" href="#">Dashboard</a>                       
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                        Exams
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="layout-static.html">Assigned Exams</a>
                        <a class="nav-link" href="layout-sidenav-light.html">Free Exams</a>
                    </nav>
                </div>
                <a class="nav-link active" href="#">                           
                    Exams Results
                </a>  
                <a class="nav-link" href="#">                           
                    Exams REview
                </a> 
                <a class="nav-link" href="#">                           
                    Create Exam
                </a> 
                 <a class="nav-link bluebtn navassbtn d-sm-none d-block" href="#">                           
                   <span class="icon-plus"></span>  Create Assessment
                </a> 
                
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Copyrights@ ExamRay 2020</div>
        </div>
    </nav>
</div>
<!--/ left nav -->