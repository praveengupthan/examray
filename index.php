<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php 
        include 'includes/styles.php'
    ?>   
    <!--/ styles -->   
    <?php 
        include "includes/objectArray.php"
    ?>
</head>
<body>

<?php
    include "includes/header.php"
?>

<!-- main -->
<!-- home banner -->
<section class="home-banner">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-6 align-self-center">
                <article>
                    <h1>Create <br> Assign <br> Valuate Assessments  </h1>
                    <p>ExamRay, a smart solution for individual, institutions and organizations learning evaluation methods. </p>
                </article>
               

                <article class="pt-3">
                    <h4 class="fwhite fbold">Let’s go digital in true way with Examray  </h4>
                </article>
              
                <div class="banner-signup">
                    <input type="text" class="banner-input" placeholder="Write Email">
                    <input type="submit" class="banner-btn" value="Signup - It's Free">
                </div>
            </div>
            <!--/ col -->
            <!-- col -->
            <div class="col-md-6">
                <img src="img/banner-examrite.svg" alt="" class="img-fluid">
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>
<!--/ home banner -->

<!-- intro 01 -->
<section class="intro-section">
    <!-- custom container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-6 align-self-center">
                <article>
                    <h2>Create Assessments </h2>
                    <p>Examray, a self-service platform, enables you to create assessments for your students, employees and trainee learning evaluation. </p>
                    <ul class="list-items py-1">
                        <li>No developer required </li>
                        <li>No designer required  </li>
                        <li>Create assessment using very user-friendly wizard-based forms. </li>
                    </ul>
                    <p class="pb-2"><i>Do you know how to operate computer? If yes, then you are going to do great at creating assessments at examray. Try now.</i></p>                   
                </article>
            </div>
            <!--/ col -->
            <!-- col -->
            <div class="col-md-6">
                <img src="img/intro01.svg" alt="" class="img-fluid">
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ custom container -->
</section>
<!--/ intro 01 -->

<!-- intro 02 -->
<section class="intro-section">
    <!-- custom container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-6 align-self-center order-md-last">
                <article>
                    <h2>Assign Assessments </h2>
                    <p>Got your assessment created and ready to go? </p>
                    <ul class="list-items py-1">
                        <li>Assign it using email ID or phone number </li>
                        <li>Assign it to multiple user at single click   </li>
                        <li>Send notifications  </li>
                        <li>Get notifications  </li>
                    </ul>                   
                    <!-- <a href="javascript:void(0)" class="bluebtn" >Read more</a> -->
                </article>
            </div>
            <!--/ col -->
            <!-- col -->
            <div class="col-md-6">
                <img src="img/intro01.svg" alt="" class="img-fluid">
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ custom container -->
</section>
<!--/ intro 02 -->

<!-- intro 03 -->
<section class="intro-section">
    <!-- custom container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-6 align-self-center">
                <article>
                    <h2>Valuate assessments  </h2>
                    <p>Set your valuation type while creating assessment  </p>
                    <ul class="list-items py-1">
                        <li>Automated </li>
                        <li>Get submission for your manual online valuation </li>                       
                    </ul>          
                    <p class="pb-2">Auto-calculation of Marks, Grade, correct answers count and wrong ones while you evaluate the submission manually </p>        
                    
                </article>
            </div>
            <!--/ col -->
            <!-- col -->
            <div class="col-md-6">
                <img src="img/intro01.svg" alt="" class="img-fluid">
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ custom container -->
</section>
<!--/ intro 03 -->

<!-- intro 02 -->
<section class="intro-section">
    <!-- custom container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-6 align-self-center order-md-last">
                <article>
                    <h2>ExamRay Practice tests</h2>
                    <ul class="list-items py-1">
                        <li>100’s of practice tests covering  </li>
                        <li>Professional assessments  </li>                       
                        <li>Certification assessments </li>
                        <li>Interviews</li>
                        <li>Curriculum based etc.</li>
                    </ul>  
                    <p>Get instant results. Retake practice tests and compare growth.  </p>                  
                </article>
            </div>
            <!--/ col -->
            <!-- col -->
            <div class="col-md-6">
                <img src="img/intro02.svg" alt="" class="img-fluid">
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ custom container -->
</section>
<!--/ intro 02 -->

<!-- how it works slider -->
<section class="how-it-works">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row justify-content-center">
            <!-- col -->
            <div class="col-md-7">
                <article class="section-title text-center">
                    <h3>See how it works</h3>                    
                </article>
                <!-- video -->
                <div class="howitworksVideo">
                    <iframe width="100%" src="https://www.youtube.com/embed/QepylOV9mEQ" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <!--/ video-->
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>
<!--/ how it works slider -->

<!-- exam your way -->
<section class="examway">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row justify-content-center d-none">
            <div class="col-md-6">
                <article class="section-title text-center">
                    <h3>Examray Your Way</h3>
                    <p>Use Trello the way your team works best. We’ve got the flexibility & features to fit any team’s style. </p>
                </article>
            </div>
        </div>
        <!--/ row -->

        <!-- row -->
        <div class="row pt-3">
            <!-- col-->
            <?php 
            for($i=0; $i<count($examway); $i++) {?>
            <div class="col-md-4 text-center">
                <img src="img/<?php echo $examway[$i][0]?>.png" alt="" class="img-fluid">
                <h3 class="h4 fblue fbold"><?php echo $examway[$i][1]?></h3>
                <p><?php echo $examway[$i][2]?></p>
            </div>
            <?php } ?>
            <!-- /col-->           
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>
<!--/ exam your way -->

<!-- clients -->
<div class="clients-home d-none">
    <!-- title -->
    <article class="section-title text-center pb-4">
        <h3>Work smarter with Examray</h3>
        <p>Companies of all shapes and sizes use Examray </p>
    </article>
    <!--/ title -->

    <!-- logos slide -->
    <div class="clients-logos">
         <!-- Swiper -->
        <div class="swiper-container client-logos">
            <div class="swiper-wrapper">
                <?php 
                for($i=0;$i<count($clientLogos);$i++) {?>
                <div class="swiper-slide">
                    <img src="img/clients/<?php echo $clientLogos[$i][0] ?>.png" alt="" class="img-fluid">
                </div>      
                <?php } ?>          
            </div>            
        </div>
    </div>
    <!--/ logos slide -->
</div>
<!--/ clients -->

<!-- planning to day -->
<div class="planning-section">
    <!-- custom container -->
    <div class="cust-container">
        <!-- row -->
        <div class="row justify-content-between">

            <!-- col -->
            <div class="col-md-4 align-self-center">
                <!-- title -->
                <article class="section-title text-center pb-4">
                    <h3>Start your journey today</h3>
                    <p class="pb-3">Sign up and join our smart community nationwide who are using EXAMRAY for their learning evaluation needs </p>
                    <a href="javascript:void(0)" class="bluebtn" >Get Started - It's Free</a>
                </article>
                <!--/ title -->
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-md-6">
                <img src="img/start-planimg.svg" alt="" class="img-fluid">
             </div>
            <!--/ col -->

        </div>
        <!--/ row -->
    </div>
    <!--/ custom container -->
</div>
<!--/ planning to day -->
<!--/ main -->

<?php 
    include "includes/footer.php"
?>

<?php include 'includes/scripts.php'?> 
</body>
</html>