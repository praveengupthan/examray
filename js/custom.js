

$(document).ready(function() {
    $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
      var $total = navigation.find('li').length;
      var $current = index+1;
      var $percent = ($current/$total) * 100;
      $('#rootwizard .progress-bar').css({width:$percent+'%'});
  }});
});




//time stest
// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2021 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("time-counter").innerHTML = hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("time-counter").innerHTML = "EXPIRED";
  }
}, 1000);


//options check

$('.optionselect').click(function(){   
    $(this).children('input').prop('checked',true);   
    $(this).toggleClass('select-option');       
});




    //accordion 
    // Hiding the panel content. If JS is inactive, content will be displayed
    $( '.panel-content' ).hide();

    // Preparing the DOM
    
    // -- Update the markup of accordion container 
    $( '.accordion' ).attr({
      role: 'tablist',
      multiselectable: 'true'
     });
  
    // -- Adding ID, aria-labelled-by, role and aria-labelledby attributes to panel content
    $( '.panel-content' ).attr( 'id', function( IDcount ) { 
      return 'panel-' + IDcount; 
    });
    $( '.panel-content' ).attr( 'aria-labelledby', function( IDcount ) { 
      return 'control-panel-' + IDcount; 
    });
    $( '.panel-content' ).attr( 'aria-hidden' , 'true' );
    // ---- Only for accordion, add role tabpanel
    $( '.accordion .panel-content' ).attr( 'role' , 'tabpanel' );
    
    // -- Wrapping panel title content with a <a href="">
    $( '.panel-title' ).each(function(i){
      
      // ---- Need to identify the target, easy it's the immediate brother
      $target = $(this).next( '.panel-content' )[0].id;
      
      // ---- Creating the link with aria and link it to the panel content
      $link = $( '<a>', {
        'href': '#' + $target,
        'aria-expanded': 'false',
        'aria-controls': $target,
        'id' : 'control-' + $target
      });
      
      // ---- Output the link
      $(this).wrapInner($link);  
      
    });
  
    // Optional : include an icon. Better in JS because without JS it have non-sense.
    $( '.panel-title a' ).append('<span class="icon">+</span>');
  
    // Now we can play with it
    $( '.panel-title a' ).click(function() {
      
      if ($(this).attr( 'aria-expanded' ) == 'false'){ //If aria expanded is false then it's not opened and we want it opened !
        
        // -- Only for accordion effect (2 options) : comment or uncomment the one you want
        
        // ---- Option 1 : close only opened panel in the same accordion
        //      search through the current Accordion container for opened panel and close it, remove class and change aria expanded value
        $(this).parents( '.accordion' ).find( '[aria-expanded=true]' ).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');
  
        // Option 2 : close all opened panels in all accordion container
        //$('.accordion .panel-title > a').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200);
        
        // Finally we open the panel, set class active for styling purpos on a and aria-expanded to "true"
        $(this).attr( 'aria-expanded' , true ).addClass( 'active' ).parent().next( '.panel-content' ).slideDown(200).attr( 'aria-hidden' , 'false');
  
      } else { // The current panel is opened and we want to close it
  
        $(this).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');;
  
      }
      // No Boing Boing
      return false;
    });



    //chart.js

// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Correct', 8],
  ['Wrong', 2],
  
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'', 'width':550, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}



//bs swiper 
var swiper = new Swiper('.howswiper-container', {
  slidesPerView: 1,
  spaceBetween: 30,
  loop: true,
autoplay: true,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

//clients logos 
var swiper = new Swiper('.client-logos', {
  slidesPerView: 1,
  spaceBetween: 10,
 
 autoplay: {
    delay: 5000,
    disableOnInteraction: true,
  },
  // init: false,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 4,
      spaceBetween: 40,
    },
    1024: {
      slidesPerView: 6,
      spaceBetween: 0,
    },
  }
});




   (function($) {  
   
    // Toggle the side navigation
    $("#sidebarToggle").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("sb-sidenav-toggled");
    });
})(jQuery);



var swiper = new Swiper('.freetests', {
  slidesPerView: 1,
  spaceBetween: 10,
 autoplay: {
    delay: 5000,
    disableOnInteraction: true,
  },
  // init: false,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 4,
      spaceBetween: 40,
    },
    1024: {
      slidesPerView: 5,
      spaceBetween: 10,
    },
  }
});


//responsive tabs
$(document).ready(function() {
  //Horizontal Tab
  $('.parentHorizontalTab').easyResponsiveTabs({
      type: 'default', //Types: default, vertical, accordion
      width: 'auto', //auto or any width like 600px
      fit: true, // 100% fit in a container
      tabidentify: 'hor_1', // The tab groups identifier
      activate: function(event) { // Callback function if tab is switched
          var $tab = $(this);
          var $info = $('#nested-tabInfo');
          var $name = $('span', $info);
          $name.text($tab.text());
          $info.show();
      }
  });

  // Child Tab
  $('#ChildVerticalTab_1').easyResponsiveTabs({
      type: 'vertical',
      width: 'auto',
      fit: true,
      tabidentify: 'ver_1', // The tab groups identifier
      activetab_bg: '#fff', // background color for active tabs in this group
      inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
      active_border_color: '#c1c1c1', // border color for active tabs heads in this group
      active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
  });

   //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

  
});



