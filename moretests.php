<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php include 'includes/styles.php'?>   
    <!--/ styles -->   
   
</head>
<body class="sb-nav-fixed user-screen">
    <?php include 'includes/objectArray.php'?>  
   <?php
    include 'includes/headerPostlogin.php';
   ?>

    <!-- main -->
    <div id="layoutSidenav">
        <?php 
            include 'includes/userAside.php';
        ?>

        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-sm-flex justify-content-between pb-2 border-bottom">
                    <h1 class="mt-2 fbold h3 mb-0 pb-0">Free Exams for Students </h1>
                    <ol class="breadcrumb mb-1 pb-0 pl-0 pl-sm-3">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Free Exams for Students</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                  

                  <!-- container fluid -->
                  <div class="container-fluid freetests">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Filter By</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Filter By</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Filter By</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Filter By</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                        <option>Filter Name</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- row -->
                        <div class="row">
                                <!-- slide -->
                                <?php 
                                for($i=0;$i<count($examListItem);$i++) { ?>
                                <div class="col-md-4 col-lg-3">
                                    <!-- card -->
                                    <div class="card test-item pb-0">
                                        <!-- card body -->
                                        <div class="card-body pb-0">
                                            <p class="text-right mb-0 pb-0">                                              
                                                <span class="label label-info"><?php echo $examListItem [$i][7]?></span>
                                            </p>
                                            <h5 class="h6 fbold">
                                                <a href="javascript:void(0)" class="fblue"><?php echo $examListItem [$i][1]?></a>
                                            </h5>
                                            <!-- <p class="fgray pb-0"><?php echo $examListItem [$i][2]?></p> -->

                                            <p class="fgray"><small>Expires on: <?php echo $examListItem [$i][3]?></small></p>
                                            <p class="d-flex justify-content-between rowp">
                                                <span class="fgray">Questions</span>
                                                <span class="fpink"><?php echo $examListItem [$i][4]?></span>
                                            </p>
                                            <p class="d-flex justify-content-between rowp">
                                                <span class="fgray">Marks</span>
                                                <span class="fpink"><?php echo $examListItem [$i][5]?></span>
                                            </p>
                                            <p class="d-flex justify-content-between rowp pb-0">
                                                <span class="fgray">Minutes</span>
                                                <span class="fpink"><?php echo $examListItem [$i][6]?></span>
                                            </p>
                                        </div>
                                        <!--/ card body -->
                                        <!-- card footer -->
                                        <div class="card-footer pt-0">
                                            <div class="pt-1 row justify-content-around">
                                                <div class="col-md-12"><a href="javascript:void(0)" class="brdlink">Assign Users</a> </div>
                                                <div class="col-md-12 text-center pt-2"> 
                                                    <a href="javascript:void(0)" class="fblue"><span class="icon-edit"></span> Edit Exam</a> 
                                                </div>
                                            </div>
                                            <p class="small"> <span>No of People Submitted</span> <b>25</b></p>
                                        </div>
                                        <!--/ card footer -->
                                    </div>
                                    <!--/ card -->
                                </div>
                                <?php } ?>
                                <!--/ slide -->
                            </div>
                            <!-- row ends -->

                            <!-- row -->
                            <div class="row py-2">
                                <div class="col-md-12">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination justify-content-end pageNation">
                                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <!--/ row -->


                  </div>
                  <!--/ container fluid -->


                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->



<?php include 'includes/scripts.php'?> 
</body>
</html>