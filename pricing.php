<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php 
        include 'includes/styles.php'
    ?>   
    <!--/ styles -->   
    <?php 
        include "includes/objectArray.php"
    ?>
</head>
<body>

<?php
    include "includes/header.php"
?>

<!-- main -->
<main class="subPage">
    <!-- header of sub page -->
    <div class="subpageHeader">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row justify-content-center">
                <div class="col-md-6 text-center">
                    <h1>Examray Your Way</h1>
                    <p>Trusted by millions, Trello powers teams all around the world. Explore which option is right for you.</p>
                </div>
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ header of sub page -->

    <!-- main of sub page -->
    <div class="mainPagae">
        <!-- container -->
        <div class="container">

            <!-- Contenedor -->
            <div class="pricing-wrapper clearfix">
              

                <div class="pricing-table">
                    <h3 class="pricing-title">Free</h3>
                    <div class="price">Free<sup>/ User a Month</sup></div>
                    
                    <ul class="table-list">
                        <li>Unlimited Exams for one month</li>
                        <li>Can assign Up to 100 users per exam</li>
                        <li>2Access to all Free practice tests</li>                        
                    </ul>
                    
                    <div class="table-buy">                       
                        <a href="javascript:void(0)" class="bluebtn">Subscribe</a>
                    </div>
                </div>
                
                <div class="pricing-table">
                    <h3 class="pricing-title">Pay AS-YOU-GO</h3>
                    <div class="price">Rs:99<sup>/ User a Month</sup></div>
                    
                    <ul class="table-list">
                        <li>Unlimited exams – One Month access</li>
                        <li>Assign exams to Unlimited users</li>
                        <li>Access to all free Practice tests</li>                       
                    </ul>
                    
                    <div class="table-buy">                     
                        <a href="javascript:void(0)" class="bluebtn">Pay Now</a>
                    </div>
                </div>

                <div class="pricing-table">
                    <h3 class="pricing-title">Annual Package</h3>
                    <div class="price">Rs:599<sup>/ Per user Per year</sup></div>
                    
                    <ul class="table-list">
                        <li>Unlimited exams – One-year access</li>
                        <li>Assign exams to Unlimited users</li>
                        <li>Access to all free Practice tests	</li>                       
                    </ul>
                    
                    <div class="table-buy">                       
                        <a href="#" class="bluebtn">Pay Now</a>
                    </div>
                </div>

                 <div class="pricing-table">
                    <h3 class="pricing-title">Enterprise / Institutions</h3>
                    <div class="price">Reach us</div>
                    
                    <ul class="table-list">
                        <li>&nbsp;</li>
                        <li>&nbsp;</li>
                        <li>&nbsp;</li>                    
                    </ul>                    
                    <div class="table-buy">                       
                        <a href="javascript:void(0)" class="bluebtn">Reach us</a>
                    </div>
                </div>


            </div>
            <!-- /Contenedor -->

              
        </div>
        <!--/ container -->
    </div>
    <!--/ main of sub page -->
</main>

<!--/ main -->

<?php 
    include "includes/footer.php"
?>

<?php include 'includes/scripts.php'?> 
</body>
</html>