<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php include 'includes/styles.php'?>   
    <!--/ styles -->   
   
</head>
<body class="sb-nav-fixed user-screen">
    <?php include 'includes/objectArray.php'?>  
   <?php
    include 'includes/headerPostlogin.php';
   ?>

    <!-- main -->
    <div id="layoutSidenav">
        <?php 
            include 'includes/userAside.php';
        ?>

        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-sm-flex justify-content-between pb-2 border-bottom">
                    <h1 class="mt-2 fbold h3 mb-0 pb-0">Dashboard</h1>
                    <ol class="breadcrumb mb-1 pb-0 pl-0 pl-sm-3">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                  

                     <!-- container fluid -->
                     <div class="container-fluid">
                        
                        <!-- exam title -->
                        <div class="title py-3">
                            <article>
                                <h2 class="h4 pt-3 fbold">Free Exams Submitted by Students</h2> 
                            </article>
                        </div>
                        <!--/ exam title -->

                          <!-- row -->
                          <div class="row pt-1">
                            <!-- job col -->
                            <?php 
                            for($i=0;$i<count($reviewItem);$i++) {?>
                            <div class="col-md-3">
                                <div class="job-col">
                                    <div class="header-jobcol d-flex justify-content-between">
                                        <div class="headin">
                                            <div class="text-center mb-2 mx-auto">
                                                <img class="sm-thumb" src="img/howimg02.jpg" alt="">
                                            </div>
                                            <h2 class="text-center"><a href="javascript:void(0)"><?php echo $reviewItem [$i][0] ?></a></h2>
                                            <p class="mb-0 text-center"><small>Submitted on: <?php echo $reviewItem [$i][1] ?></small></p>
                                        </div>                                       
                                    </div>
                                    <ul class="nav justify-content-center">
                                        <li class="nav-item">
                                            <a><span class="fgray">Questions:</span> <?php echo $reviewItem [$i][2] ?></a>
                                        </li>
                                        <li class="nav-item">
                                            <a><span class="fgray">Attempt:</span> <?php echo $reviewItem [$i][3] ?></a>
                                        </li>
                                        <li class="nav-item">
                                            <a><span class="fgray">Skipped:</span>  <?php echo $reviewItem [$i][4] ?> </a>
                                        </li>
                                    </ul>
                                    <p class="pt-3 text-center d-block">
                                        <a href="javascript:void(0)" class="bluebtn">View Exam</a>                                                
                                    </p>
                                    <p class="salary text-center">
                                        Review <span class="fblue fbold"><?php echo $reviewItem [$i][5] ?></span>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>
                            <!--/ job col -->
                        </div>
                        <!--/ row -->

                     
                           
                       

                    </div>
                    <!--/ container fluid -->


                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->



<?php include 'includes/scripts.php'?> 
</body>
</html>