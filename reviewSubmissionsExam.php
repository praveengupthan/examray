<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php include 'includes/styles.php'?>   
    <!--/ styles -->   
   
</head>
<body class="sb-nav-fixed user-screen">
    <?php include 'includes/objectArray.php'?>  
   <?php
    include 'includes/headerPostlogin.php';
   ?>

    <!-- main -->
    <div id="layoutSidenav">
        <?php 
            include 'includes/userAside.php';
        ?>

        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-sm-flex justify-content-between pb-2 border-bottom">
                    <h1 class="mt-2 fbold h3 mb-0 pb-0">Dashboard</h1>
                    <ol class="breadcrumb mb-1 pb-sm-0 pl-0 pl-sm-3 ">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                  

                     <!-- container fluid -->
                     <div class="container-fluid">
                        
                        <!-- exam title -->
                        <div class="title py-3">
                            <article>
                                <h2 class="h4 pt-3 fbold">Exam Submitted by Praveen</h2> 
                            </article>
                        </div>
                        <!--/ exam title -->

                        <!-- row -->
                        <div class="row">
                        <!-- col left -->
                        <div class="col-md-8">
                            <div class="whitebg mb-2 px-3">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>Filter By</option>
                                                <option>Filter Name</option>
                                                <option>Filter Name</option>
                                                <option>Filter Name</option>
                                                <option>Filter Name</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>Filter By</option>
                                                <option>Filter Name</option>
                                                <option>Filter Name</option>
                                                <option>Filter Name</option>
                                                <option>Filter Name</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <!-- submissionRow -->
                             <div class="py-2 border-bottom mb-2 submissionRow">  

                                <!-- question and options -->
                                <div class="qandans pt-4">
                                    <p class="qnumberinquestion py-2"><b>Question 1:</b></p>
                                    <p class="question pb-2">Staying healthy helps you manage ADHD better and also helps your body respond more readily to any extra stress. Children, teens, and adults with ADHD can all take these steps to maintain health:</p>
                                </div>
                                <!--/ question options -->
                                  
                                <div class="exam-View">
                                    <!-- options -->
                                    <ul class="options">
                                        <li class="optionselect">
                                            <input type="radio" name="" checked>
                                            <span>How is nose acne treated?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>What ingredients treat acne?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>How is nose acne treated?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>What is acne?</span>
                                        </li>
                                    </ul>
                                    <!--/ options -->
                                </div>
                                    
                                      
                                <!-- form group -->
                                <div class="form-group row px-0">                                          
                                    <div class="col-sm-3 col-6">
                                        <div class="form-check">                                       
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Correct
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-3 col-6">
                                        <div class="form-check">                                       
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Wrong
                                            </label>
                                        </div>
                                    </div>  
                                        
                                        <div class="col-sm-2 d-flex pt-3 pt-sm-0">
                                            <label class="col-form-label">Marks</label>                                   
                                            <div class="input-group ml-3">
                                                <input type="number" class="form-control" style="margin-top:-2px;" placeholder="Marks"/>
                                            </div>                                              
                                        </div>                                      
                                    </div>
                                    <!--/ form group -->
                                </div>
                                <!--/ submissionRow -->


                                 <!-- submissionRow -->
                             <div class="py-2 border-bottom mb-2 submissionRow">  

                                <!-- question and options -->
                                <div class="qandans pt-4">
                                    <p class="qnumberinquestion py-2"><b>Question 2:</b></p>
                                    <p class="question pb-2">Staying healthy helps you manage ADHD better and also helps your body respond more readily to any extra stress. Children, teens, and adults with ADHD can all take these steps to maintain health:</p>
                                </div>
                                <!--/ question options -->
                                
                                <div class="exam-View">
                                    <!-- options -->
                                    <ul class="options">
                                        <li class="optionselect">
                                            <input type="radio" name="" checked>
                                            <span>How is nose acne treated?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>What ingredients treat acne?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>How is nose acne treated?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>What is acne?</span>
                                        </li>
                                    </ul>
                                    <!--/ options -->
                                </div>
                                    
                                    
                                <!-- form group -->
                                <div class="form-group row px-0">                                          
                                    <div class="col-sm-3 col-6">
                                        <div class="form-check">                                       
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Correct
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-3 col-6">
                                        <div class="form-check">                                       
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Wrong
                                            </label>
                                        </div>
                                    </div>  
                                        
                                        <div class="col-sm-2 d-flex pt-3 pt-sm-0">
                                            <label class="col-form-label">Marks</label>                                   
                                            <div class="input-group ml-3">
                                                <input type="number" class="form-control" style="margin-top:-2px;" placeholder="Marks"/>
                                            </div>                                              
                                        </div>                                      
                                    </div>
                                    <!--/ form group -->
                                </div>
                                <!--/ submissionRow -->



                                <!-- submissionRow -->
                                <div class="py-2 border-bottom mb-2 submissionRow">  

                                <!-- question and options -->
                                <div class="qandans pt-4">
                                    <p class="qnumberinquestion py-2"><b>Question 3:</b></p>
                                    <p class="question pb-2">Staying healthy helps you manage ADHD better and also helps your body respond more readily to any extra stress. Children, teens, and adults with ADHD can all take these steps to maintain health:</p>
                                </div>
                                <!--/ question options -->
                                
                                <div class="exam-View">
                                    <!-- options -->
                                    <ul class="options">
                                        <li class="optionselect">
                                            <input type="radio" name="" checked>
                                            <span>How is nose acne treated?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>What ingredients treat acne?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>How is nose acne treated?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>What is acne?</span>
                                        </li>
                                    </ul>
                                    <!--/ options -->
                                </div>
                                    
                                    
                                <!-- form group -->
                                <div class="form-group row px-0">                                          
                                    <div class="col-sm-3 col-6">
                                        <div class="form-check">                                       
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Correct
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-3 col-6">
                                        <div class="form-check">                                       
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Wrong
                                            </label>
                                        </div>
                                    </div>  
                                        
                                        <div class="col-sm-2 d-flex pt-3 pt-sm-0">
                                            <label class="col-form-label">Marks</label>                                   
                                            <div class="input-group ml-3">
                                                <input type="number" class="form-control" style="margin-top:-2px;" placeholder="Marks"/>
                                            </div>                                              
                                        </div>                                      
                                    </div>
                                    <!--/ form group -->
                                </div>
                                <!--/ submissionRow -->
                            </div>
                            <!--/ col left -->

                            <!-- col right -->
                            <div class="col-md-4 marksTable">
                                <div class="whitebg p-3 sticky-top">
                                    <table class="table  table-striped w-100">
                                        <tr>
                                            <td>Total Number of Questions</td>
                                            <td>:</td>
                                            <td><b class="fblue">25</b></td>
                                        </tr>
                                        <tr>
                                            <td>Total Correct</td>
                                            <td>:</td>
                                            <td><b class="fblue">15</b></td>
                                        </tr>
                                        <tr>
                                            <td>Total Wrong</td>
                                            <td>:</td>
                                            <td><b class="fblue">5</b></td>
                                        </tr>
                                        <tr>
                                            <td>Marks in %</td>
                                            <td>:</td>
                                            <td><b class="fblue">75%</b></td>
                                        </tr>
                                    </table>

                                    <p class="text-center pt-3"><a class="bluebtn" href="javascript:void(0)">Send Report</a></p>
                                </div>
                            </div>
                            <!--/ col right -->
                        </div>
                        <!--/ row -->

                    </div>
                    <!--/ container fluid -->
                    
                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->



<?php include 'includes/scripts.php'?> 
</body>
</html>