<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php include 'includes/styles.php'?>   
    <!--/ styles -->   
</head>
<body>

<!-- signin -->
<section class="sign">
    <!-- container fluid -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-6">
                <!-- signin block -->
                <div class="signin-block">
                    <a href="index.php" class="signbrand">
                        <img src="img/logo-color.svg" alt="">
                    </a>
                    <!-- sign section -->
                    <div class="sign-section">
                        <p class="text-center fbold">Sign up for your account</p>
                        <form class="form-sign" method="">
                            
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" placeholder="Enter Email">
                                </div>
                            </div>      
                            <p>By signing up, you confirm that you've read and accepted our <a href="javascript:void(0)">Terms of Service</a> and <a href="javascript:void(0)">Privacy Policy</a></p>                      
                            <input type="submit" value="Continue" class="bluebtn w-100">
                        </form>
                        <p class="text-center pt-3">OR</p>

                        <!-- social login-->
                        <div class="social-login">
                            <a href="javascript:void(0)"><span class="icon-google-plus icomoon"></span>  Continue with Google</a>
                           
                        </div>
                        <!--/ social login-->

                        <p class="pt-3">
                        Already have an account?   <a href="signin.php">Sign in</a>
                        </p>
                    </div>
                    <!--/ sign section -->
                </div>
                <!--/ signin block -->
            </div>
            <!--/ col -->

            <!-- col -->
            <div class="col-md-6 d-none d-sm-block">
                <img src="img/signinimg.svg" alt="" class="img-fluid">
            </div>
            <!--/ col -->


        </div>
        <!--/ row -->
    </div>
    <!--/ container fluid -->
</section>
<!--/ sign -->

<?php include 'includes/scripts.php'?> 
</body>
</html>