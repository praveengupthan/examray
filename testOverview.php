<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Write Test</title>
    <?php include 'includes/styles.php'?>
    

    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
	</style>
</head>
<body>
   <?php include 'includes/examHeader.php'?>
    <!-- main -->
    <main>
        <!-- container -->
        <div class="container">

         <!-- row -->
         <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-10">

                    <!-- question block -->
                    <div class="queblock">
                        <h6 class="h6 pb-3 border-bottom mb-3">Preparation Exam: Microsoft Azure Fundamentals (AZ-960)-II-Results</h6>

                        <!-- version -->
                        <div class="version-section">
                            <h6 class="h6">Version 01</h6>
                            <p class="spans-list">
                                <span>50 Questions </span>
                                <span>1 Hour 30 Min</span>
                                <span>70% correct required pass </span>
                            </p>

                             <!-- accordion -->
                             <div class="accord">

                                <div class="accordion text-reviewaccordion">
                                    <!-- accord -->
                                    <h5 class="panel-title">
                                        <span class="text-danger">Failed</span>
                                        <span>0% Correct</span>
                                        <span>2 Minutes</span>
                                        <span>10-10-2020</span>
                                    </h5>
                                    <div class="panel-content">
                                       <!-- row -->
                                       <div class="row">
                                           <!-- col -->
                                           <div class="col-sm-7 col-12">
                                                <div id="piechart"></div>
                                           </div>
                                           <!--/ col -->
                                           <!-- col -->
                                           <div class="col-sm-5 col-12 align-self-center">
                                               <h5 class="h6">
                                                    <span class="fbold">Attempt 4,</span> Passed, (Marks: 55/60) 
                                                </h5>
                                                <h2 class="h2">80%  <small>Correct (45/50) </small></h2>
                                               
                                                <p>
                                                    <small>22 minutes</small>
                                                </p>
                                                <p>March 16, 2020 4:38 PM</p>
                                                <a class="brd_link" href="javascript:void(0)">Review Questions</a>
                                           </div>
                                           <!--/ col -->
                                       </div>
                                       <!--/row -->
                                    </div>
                                    <!--/ accord -->

                                     <!-- accord -->
                                     <h5 class="panel-title">
                                        <span class="text-danger">Failed</span>
                                        <span>0% Correct</span>
                                        <span>2 Minutes</span>
                                        <span>10-10-2020</span>
                                    </h5>
                                    <div class="panel-content">
                                       <!-- row -->
                                       <div class="row">
                                           <!-- col -->
                                           <div class="col-md-7">
                                                <div id="piechart"></div>
                                           </div>
                                           <!--/ col -->
                                           <!-- col -->
                                           <div class="col-md-5 align-self-center">
                                               <h5 class="h6">
                                                    <span class="fbold">Attempt 4,</span> Passed, (70% required to pass) 
                                                </h5>
                                                <h2 class="h2">80%  <small>Correct (45/50) </small></h2>
                                               
                                                <p>
                                                    <small>22 minutes</small>
                                                </p>
                                                <p>March 16, 2020 4:38 PM</p>
                                                <a class="brd_link" href="javascript:void(0)">Review Questions</a>
                                           </div>
                                           <!--/ col -->
                                       </div>
                                       <!--/row -->
                                    </div>
                                    <!--/ accord -->

                                     <!-- accord -->
                                     <h5 class="panel-title">
                                        <span class="text-success">Passed</span>
                                        <span>25% Correct</span>
                                        <span>22 Minutes</span>
                                        <span>22-10-2020</span>
                                    </h5>
                                    <div class="panel-content">
                                       <!-- row -->
                                       <div class="row">
                                           <!-- col -->
                                           <div class="col-md-7">
                                                <div id="piechart"></div>
                                           </div>
                                           <!--/ col -->
                                           <!-- col -->
                                           <div class="col-md-5 align-self-center">
                                               <h5 class="h6">
                                                    <span class="fbold">Attempt 4,</span> Passed, (70% required to pass) 
                                                </h5>
                                                <h2 class="h2">80%  <small>Correct (45/50) </small></h2>
                                               
                                                <p>
                                                    <small>22 minutes</small>
                                                </p>
                                                <p>March 16, 2020 4:38 PM</p>
                                                <a class="brd_link" href="javascript:void(0)">Review Questions</a>
                                           </div>
                                           <!--/ col -->
                                       </div>
                                       <!--/row -->
                                    </div>
                                    <!--/ accord -->
                                    
                                   
                                </div>
                            </div>
                            <!--/ accorion -->


                        </div>
                        <!--/ version -->

                    </div>
                    <!--/ questionblock -->
                 

                </div>
                <!--/ col -->
            </div>
            <!--/ row -->

           
        </div>
        <!--/ container -->
    </main>
    <!--/ main -->
    <!-- <script src="js/Chart.min.js"></script>
    <script src="js/utils.js"></script> -->
    <?php include 'includes/scripts.php'?> 


    
   
</body>
</html>