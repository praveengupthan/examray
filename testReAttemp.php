<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Write Test</title>
    <?php include 'includes/styles.php'?> 
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
	</style>
</head>
<body>
   <?php include 'includes/examHeader.php'?>
    <!-- main -->
    <main>
        <!-- container -->
        <div class="container">

         <!-- row -->
         <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-8">

                    <!-- Results  -->
                    <div class="queblock">
                        <h6 class="h6 pb-3 border-bottom mb-3">Preparation Exam: Microsoft Azure Fundamentals (AZ-960)-II-Results</h6>
                        <p>You Can ReAttempt Skipped Questions</p>

                        <p class="text-right">
                            <a href="testOverview.php"><span class="icon-arrow-left icomoon"></span> Return to Test Review</a>
                        </p>  
                        
                        <!-- results block -->
                        <div class="results-block">
                            <div class="d-flex justify-content-between">
                                <h1 class="h5">Attempt 4</h1>
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>In correct</option>
                                        <option>Correct</option>
                                        <option>Skipped</option>
                                    </select>
                                </div>
                            </div>

                            <!-- accordion -->
                            <div class="accord testResults">
                                <div class="accordion">
                                    <!-- accord-->
                                    <div class="panel-title p-2">
                                        <p class="qnumberinquestion pb-0">Q1: <span class="text-danger fbold">Incorrect</span></p>
                                        <p class="question pb-0 mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea officiis, quisquam deleniti:</p>
                                    </div>
                                    <div class="panel-content">
                                       <!-- options -->
                                        <ul class="options-results">
                                            <li class="optionselectresult correct-opt">
                                                <input type="checkbox" name="">
                                                <span>How is nose acne treated?</span>
                                                <span class="req-opt ans-write">Correct</span>
                                            </li>
                                            <li class="optionselectresult">
                                                <input type="checkbox" name="">
                                                <span>What ingredients treat acne?</span>
                                            </li>
                                            <li class="optionselectresult wrong-opt disabled">
                                                <input type="checkbox" name="" checked>
                                                <span>How is nose acne treated?</span>
                                                <span class="req-opt ans-wrong">Wrong</span>
                                            </li>
                                            <li class="optionselectresult ">
                                                <input type="checkbox" name="">
                                                <span>What is acne?</span>
                                            </li>
                                        </ul>
                                        <!--/ options -->
                                    </div>
                                    <!-- accord -->
                                    
                                      <!-- accord-->
                                      <div class="panel-title p-2">
                                        <p class="qnumberinquestion pb-0">Question 2: <span class="text-success fbold">Correct</span></p>
                                        <p class="question pb-0 mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    </div>
                                    <div class="panel-content">
                                       <!-- options -->
                                        <ul class="options-results">
                                            <li class="optionselectresult correct-opt disabled">
                                                <input type="checkbox" name="" checked>
                                                <span>How is nose acne treated?</span>
                                                <span class="req-opt ans-write">Correct</span>
                                            </li>
                                            <li class="optionselectresult">
                                                <input type="checkbox" name="">
                                                <span>Lorem ipsum dolor sit amet.?</span>
                                            </li>
                                            <li class="optionselectresult">
                                                <input type="checkbox" name="">
                                                <span>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit, ratione?</span>                                                
                                            </li>
                                            <li class="optionselectresult ">
                                                <input type="checkbox" name="">
                                                <span>What is acne?</span>
                                            </li>
                                        </ul>
                                        <!--/ options -->
                                    </div>
                                    <!-- accord -->                                    
                                      <!-- accord-->
                                      <div class="panel-title p-2">
                                        <p class="qnumberinquestion pb-0">Question 3: <span class="text-primary fbold">Skipped</span></p>
                                        <p class="question pb-0 mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    </div>
                                    <div class="panel-content">
                                       <!-- options -->
                                        <ul class="options-results reattempresults">
                                            <li class="">
                                                <input type="checkbox" name="">
                                                <span>How is nose acne treated?</span>
                                            </li>
                                            <li class="">
                                                <input type="checkbox" name="">
                                                <span>Lorem ipsum dolor sit amet.?</span>
                                            </li>
                                            <li class="">
                                                <input type="checkbox" name="">
                                                <span>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit, ratione?</span>                                                
                                            </li>
                                            <li class=" ">
                                                <input type="checkbox" name="">
                                                <span>What is acne?</span>
                                            </li>
                                        </ul>
                                        <!--/ options -->
                                    </div>
                                    <!-- accord -->
                                      <!-- accord-->
                                      <div class="panel-title p-2">
                                        <p class="qnumberinquestion pb-0">Question 2: <span class="text-success fbold">Correct</span></p>
                                        <p class="question pb-0 mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    </div>
                                    <div class="panel-content">
                                       <!-- options -->
                                        <ul class="options-results">
                                            <li class="optionselectresult correct-opt disabled">
                                                <input type="checkbox" name="" checked>
                                                <span>How is nose acne treated?</span>
                                                <span class="req-opt ans-write">Correct</span>
                                            </li>
                                            <li class="optionselectresult">
                                                <input type="checkbox" name="">
                                                <span>Lorem ipsum dolor sit amet.?</span>
                                            </li>
                                            <li class="optionselectresult">
                                                <input type="checkbox" name="">
                                                <span>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit, ratione?</span>                                                
                                            </li>
                                            <li class="optionselectresult ">
                                                <input type="checkbox" name="">
                                                <span>What is acne?</span>
                                            </li>
                                        </ul>
                                        <!--/ options -->
                                    </div>
                                    <!-- accord -->

                                      <!-- accord-->
                                      <div class="panel-title p-2">
                                        <p class="qnumberinquestion pb-0">Question 2: <span class="text-success fbold">Correct</span></p>
                                        <p class="question pb-0 mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    </div>
                                    <div class="panel-content">
                                       <!-- options -->
                                        <ul class="options-results">
                                            <li class="optionselectresult correct-opt disabled">
                                                <input type="checkbox" name="" checked>
                                                <span>How is nose acne treated?</span>
                                                <span class="req-opt ans-write">Correct</span>
                                            </li>
                                            <li class="optionselectresult">
                                                <input type="checkbox" name="">
                                                <span>Lorem ipsum dolor sit amet.?</span>
                                            </li>
                                            <li class="optionselectresult">
                                                <input type="checkbox" name="">
                                                <span>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit, ratione?</span>                                                
                                            </li>
                                            <li class="optionselectresult ">
                                                <input type="checkbox" name="">
                                                <span>What is acne?</span>
                                            </li>
                                        </ul>
                                        <!--/ options -->
                                    </div>
                                    <!-- accord -->
                                </div>
                            </div>
                            <!--/ accordion -->
                        </div>
                        <!--/ results block -->
                        <!-- row -->
                        <div class="row justify-content-center py-5">
                            <div class="col-md-4 text-center">
                                <a class="bluebtn" href="moretests.php">Submit Answers</a>
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ Results  -->
                 

                </div>
                <!--/ col -->
            </div>
            <!--/ row -->

           
        </div>
        <!--/ container -->
    </main>
    <!--/ main -->
    <!-- <script src="js/Chart.min.js"></script>
    <script src="js/utils.js"></script> -->
    <?php include 'includes/scripts.php'?> 


    
   
</body>
</html>