<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php include 'includes/styles.php'?>   
    <!--/ styles -->   
   
</head>
<body class="sb-nav-fixed user-screen">
    <?php include 'includes/objectArray.php'?>  
   <?php
    include 'includes/headerPostlogin.php';
   ?>
    <!-- main -->
    <div id="layoutSidenav">
        <?php 
            include 'includes/userAside.php';
        ?>
        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-sm-flex justify-content-between pb-2 border-bottom">
                    <h1 class="mt-2 fbold h3 mb-0 pb-0 pl-0 pl-sm-3">Dashboard</h1>
                    <ol class="breadcrumb mb-1 pb-0">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                    <!-- container fluid -->
                    <div class="container-fluid">
                   
                        <!-- exam title -->
                        <div class="title d-sm-flex justify-content-between">
                            <article>
                                <h2 class="h4 pt-3 fbold">Your Assessement</h2>                               
                            </article>
                            <p class="text-right pt-3"><a class="bluebtn" href="javascript:void(0)"><span class="icon-plus"></span> Create Exam</a></p>
                        </div>
                        <!--/ exam title -->

                       <!-- slider -->
                       <div class="swiper-container freetests">                       
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Filter By</option>
                                            <option>Filter Name</option>
                                            <option>Filter Name</option>
                                            <option>Filter Name</option>
                                            <option>Filter Name</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="row">
                                <!-- slide -->
                                <?php 
                                for($i=0;$i<8;$i++) { ?>
                                <div class="col-md-3">
                                    <!-- card -->
                                    <div class="card test-item pb-0">
                                        <!-- card body -->
                                        <div class="card-body pb-0">
                                            <p class="text-right mb-0 pb-0">                                              
                                                <span class="label label-primary"><?php echo $examListItem [$i][0]?></span>
                                            </p>
                                            <h5 class="h6 fbold">
                                                <a href="javascript:void(0)" class="fblue"><?php echo $examListItem [$i][1]?></a>
                                            </h5>
                                            <!-- <p class="fgray pb-0"><?php echo $examListItem [$i][2]?></p> -->

                                            <p class="fgray"><small>Expires on: <?php echo $examListItem [$i][3]?></small></p>
                                            <p class="d-flex justify-content-between rowp">
                                                <span class="fgray">Questions</span>
                                                <span class="fpink"><?php echo $examListItem [$i][4]?></span>
                                            </p>
                                            <p class="d-flex justify-content-between rowp">
                                                <span class="fgray">Marks</span>
                                                <span class="fpink"><?php echo $examListItem [$i][5]?></span>
                                            </p>
                                            <p class="d-flex justify-content-between rowp pb-0">
                                                <span class="fgray">Minutes</span>
                                                <span class="fpink"><?php echo $examListItem [$i][6]?></span>
                                            </p>
                                        </div>
                                        <!--/ card body -->
                                        <!-- card footer -->
                                        <div class="card-footer text-center pt-0">
                                            <p class="pt-1 text-center">
                                                <a href="javascript:void(0)" class="brdlink mx-auto">Edit</a>                                           
                                            </p>
                                            <p class="pt-1 text-center">
                                                <a href="javascript:void(0)" class="brdlink mx-auto">Start Now</a>                                           
                                            </p>
                                            <p class="text-center small"> <span>No of People Submitted</span> <b>25</b></p>
                                        </div>
                                        <!--/ card footer -->
                                    </div>
                                    <!--/ card -->
                                </div>
                                <?php } ?>
                                <!--/ slide -->
                            </div>
                            <!-- row ends -->

                            <!-- row -->
                            <div class="row justify-content-center py-4 border-bottom">
                                <!-- col -->
                                <div class="col-md-6 text-center">
                                    <a href="javascript:void(0)" class="bluebtn mx-auto">View More</a> 
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->




                             <!-- exam title -->
                        <div class="title d-flex justify-content-between">
                            <article>
                                <h2 class="h4 pt-3 fbold px-2">Free Assessement</h2>                               
                            </article>                           
                        </div>
                        <!--/ exam title -->
                        
                             <!-- row -->
                             <div class="row">
                                <!-- slide -->
                                <?php 
                                for($i=0;$i<8;$i++) { ?>
                                <div class="col-md-3">
                                    <!-- card -->
                                    <div class="card test-item pb-0">
                                        <!-- card body -->
                                        <div class="card-body pb-0">
                                            <p class="text-right mb-0 pb-0">                                              
                                                <span class="label label-info"><?php echo $examListItem [$i][7]?></span>
                                            </p>
                                            <h5 class="h6 fbold">
                                                <a href="javascript:void(0)" class="fblue"><?php echo $examListItem [$i][1]?></a>
                                            </h5>
                                            <!-- <p class="fgray pb-0"><?php echo $examListItem [$i][2]?></p> -->

                                            <p class="fgray"><small>Expires on: <?php echo $examListItem [$i][3]?></small></p>
                                            <p class="d-flex justify-content-between rowp">
                                                <span class="fgray">Questions</span>
                                                <span class="fpink"><?php echo $examListItem [$i][4]?></span>
                                            </p>
                                            <p class="d-flex justify-content-between rowp">
                                                <span class="fgray">Marks</span>
                                                <span class="fpink"><?php echo $examListItem [$i][5]?></span>
                                            </p>
                                            <p class="d-flex justify-content-between rowp pb-0">
                                                <span class="fgray">Minutes</span>
                                                <span class="fpink"><?php echo $examListItem [$i][6]?></span>
                                            </p>
                                        </div>
                                        <!--/ card body -->
                                        <!-- card footer -->
                                        <div class="card-footer text-center pt-0">
                                            <p class="pt-1 text-center">
                                                <a href="javascript:void(0)" class="brdlink mx-auto">Start Now</a>                                           
                                            </p>
                                            <p class="text-center small"> <span>No of People Submitted</span> <b>25</b></p>
                                        </div>
                                        <!--/ card footer -->
                                    </div>
                                    <!--/ card -->
                                </div>
                                <?php } ?>
                                <!--/ slide -->
                            </div>
                            <!-- row ends -->

                            <!-- row -->
                            <div class="row justify-content-center py-4">
                                <!-- col -->
                                <div class="col-md-6 text-center">
                                    <a href="javascript:void(0)" class="bluebtn mx-auto">View More</a> 
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->
                           
                        </div>
                        <!--/ slider -->
                    </div>
                    <!--/ container fluid -->
                  

                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->



<?php include 'includes/scripts.php'?> 
</body>
</html>