<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Ray</title>
    <!-- styles -->
    <?php include 'includes/styles.php'?>   
    <!--/ styles -->   
   
</head>
<body class="sb-nav-fixed user-screen">
    <?php include 'includes/objectArray.php'?>  
   <?php
    include 'includes/headerPostlogin.php';
   ?>
    <!-- main -->
    <div id="layoutSidenav">
        <?php 
            include 'includes/userAside.php';
        ?>
        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-sm-flex justify-content-between pb-2 border-bottom">
                    <h1 class="mt-2 fbold h3 mb-0 pb-0 pl-0 pl-sm-3">Subscription</h1>
                    <ol class="breadcrumb mb-1 pb-0">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Subscription</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                    <!-- container fluid -->
                    <div class="container-fluid">
                   
               <!-- Contenedor -->
            <div class="pricing-wrapper clearfix mx-auto">
              

                <div class="pricing-table">
                    <h3 class="pricing-title">Free</h3>
                    <div class="price">Free<sup>/ User a Month</sup></div>
                    
                    <ul class="table-list">
                        <li>Unlimited Exams for one month</li>
                        <li>Can assign Up to 100 users per exam</li>
                        <li>2Access to all Free practice tests</li>                        
                    </ul>
                    
                    <div class="table-buy">                       
                        <a href="javascript:void(0)" class="bluebtn">Subscribe</a>
                    </div>
                </div>
                
                <div class="pricing-table">
                    <h3 class="pricing-title">Pay AS-YOU-GO</h3>
                    <div class="price">Rs:99<sup>/ User a Month</sup></div>
                    
                    <ul class="table-list">
                        <li>Unlimited exams – One Month access</li>
                        <li>Assign exams to Unlimited users</li>
                        <li>Access to all free Practice tests</li>                       
                    </ul>
                    
                    <div class="table-buy">                     
                        <a href="javascript:void(0)" class="bluebtn">Pay Now</a>
                    </div>
                </div>

                <div class="pricing-table">
                    <h3 class="pricing-title">Annual Package</h3>
                    <div class="price">Rs:599<sup>/ Per user Per year</sup></div>
                    
                    <ul class="table-list">
                        <li>Unlimited exams – One-year access</li>
                        <li>Assign exams to Unlimited users</li>
                        <li>Access to all free Practice tests	</li>                       
                    </ul>
                    
                    <div class="table-buy">                       
                        <a href="#" class="bluebtn">Pay Now</a>
                    </div>
                </div>

                 <div class="pricing-table">
                    <h3 class="pricing-title">Enterprise / Institutions</h3>
                    <div class="price">Reach us</div>
                    
                    <ul class="table-list">
                        <li>&nbsp;</li>
                        <li>&nbsp;</li>
                        <li>&nbsp;</li>                    
                    </ul>                    
                    <div class="table-buy">                       
                        <a href="javascript:void(0)" class="bluebtn">Reach us</a>
                    </div>
                </div>


            </div>
            <!-- /Contenedor -->

                       

                     </div>
                     <!--/ container fluid -->                  

                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->



<?php include 'includes/scripts.php'?> 
</body>
</html>