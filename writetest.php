<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Write Test</title>

    <?php include 'includes/styles.php'?>
</head>
<body>

   <?php include 'includes/examHeader.php'?>

    <!-- main -->
    <main>
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-8">

                    <!-- root wizard -->
                    <div id="rootwizard" class="step-wizard">
                        <div class="navbar">
                          <div class="navbar-inner">
                            <div class="container">
                                <ul>
                                    <li><a href="#tab1" data-toggle="tab">First</a></li>
                                    <li><a href="#tab2" data-toggle="tab">Second</a></li>
                                    <li><a href="#tab3" data-toggle="tab">Third</a></li>
                                    <li><a href="#tab4" data-toggle="tab">Forth</a></li>
                                    <li><a href="#tab5" data-toggle="tab">Fifth</a></li>
                                    <li><a href="#tab6" data-toggle="tab">Sixth</a></li>
                                    <li><a href="#tab7" data-toggle="tab">Seventh</a></li>
                                </ul>
                            </div>
                          </div>
                        </div>
                        <!-- test heder -->                       
                        <!--/ test header -->
                        <div id="bar" class="progress">                          
                          <!-- progress bar -->
                          <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" 
                          style="width: 0%;"></div>
                          <!-- progress bar -->                         
                        </div>
                        <!-- progress 2 -->
                        <div class="d-flex justify-content-between timeheader">
                          <!-- count -->
                          <p class="qnumber">1/50</p>
                          <!--/ count -->
                          <!-- time -->
                           <div class="time-test">
                                <span class="icon-clock-o"></span>
                                <p class="d-inline" id="time-counter"></p>
                                <a href="javascript:void(0)" class="timekeys">
                                    <span class="icon-pause icomoon"></span>
                                    <span class="icon-stop icomoon"></span>
                                </a>
                           </div>
                           <!--/time -->
                        </div>
                        <!-- progress 2 -->
                        <div class="tab-content">
                            <div class="tab-pane" id="tab1">
                                <!-- question and options -->
                                <div class="qandans">
                                    <p class="qnumberinquestion">Question 1:</p>
                                    <p class="question">Staying healthy helps you manage ADHD better and also helps your body respond more readily to any extra stress. Children, teens, and adults with ADHD can all take these steps to maintain health:</p>

                                    <!-- options -->
                                    <ul class="options">
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>How is nose acne treated?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>What ingredients treat acne?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>How is nose acne treated?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>What is acne?</span>
                                        </li>
                                    </ul>
                                    <!--/ options -->
                                </div>
                                <!--/ question options -->
                            </div>
                            <div class="tab-pane" id="tab2">
                             <!-- question and options -->
                             <div class="qandans">
                                <p class="qnumberinquestion">Question 2:</p>
                                <p class="question">What ingredients treat acne?:</p>

                                <!-- options -->
                                <ul class="options">
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>How is nose acne treated?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>What ingredients treat acne?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>How is nose acne treated?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>What is acne?</span>
                                    </li>
                                </ul>
                                <!--/ options -->
                            </div>
                            <!--/ question options -->
                            </div>
                            <div class="tab-pane" id="tab3">
                            <!-- question and options -->
                             <div class="qandans">
                                <p class="qnumberinquestion">Question 3:</p>
                                <p class="question">What are the side effects of oral antibiotics for acne?</p>

                                <!-- options -->
                                <ul class="options">
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>How is nose acne treated?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>What ingredients treat acne?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>How is nose acne treated?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>What is acne?</span>
                                    </li>
                                </ul>
                                <!--/ options -->
                            </div>
                            <!--/ question options -->
                            </div>
                            <div class="tab-pane" id="tab4">
                                <!-- question and options -->
                                <div class="qandans">
                                    <p class="qnumberinquestion">Question 4:</p>
                                    <p class="question">Staying healthy helps you manage ADHD better and also helps your body respond more readily to any extra stress. Children, teens, and adults with ADHD can all take these steps to maintain health:</p>

                                    <!-- options -->
                                    <ul class="options">
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>How is nose acne treated?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>What ingredients treat acne?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>How is nose acne treated?</span>
                                        </li>
                                        <li class="optionselect">
                                            <input type="radio" name="">
                                            <span>What is acne?</span>
                                        </li>
                                    </ul>
                                    <!--/ options -->
                                </div>
                                <!--/ question options -->
                            </div>
                            <div class="tab-pane" id="tab5">
                             <!-- question and options -->
                             <div class="qandans">
                                <p class="qnumberinquestion">Question 5:</p>
                                <p class="question">What ingredients treat acne?:</p>

                                <!-- options -->
                                <ul class="options">
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>How is nose acne treated?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>What ingredients treat acne?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>How is nose acne treated?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>What is acne?</span>
                                    </li>
                                </ul>
                                <!--/ options -->
                            </div>
                            <!--/ question options -->
                            </div>
                            <div class="tab-pane" id="tab6">
                                <!-- question and options -->
                             <div class="qandans">
                                <p class="qnumberinquestion">Question 6:</p>
                                <p class="question">What are the side effects of oral antibiotics for acne?</p>

                                <!-- options -->
                                <ul class="options">
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>How is nose acne treated?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>What ingredients treat acne?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>How is nose acne treated?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>What is acne?</span>
                                    </li>
                                </ul>
                                <!--/ options -->
                            </div>
                            <!--/ question options -->
                            </div>
                            <div class="tab-pane" id="tab7">
                                <!-- question and options -->
                             <div class="qandans">
                                <p class="qnumberinquestion">Question 7:</p>
                                <p class="question">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cumque illum exercitationem perspiciatis provident iste eveniet repudiandae culpa at, molestias pariatur!</p>

                                <!-- options -->
                                <ul class="options">
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>How is nose acne treated?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>What ingredients treat acne?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>How is nose acne treated?</span>
                                    </li>
                                    <li class="optionselect">
                                        <input type="checkbox" name="">
                                        <span>What is acne?</span>
                                    </li>
                                </ul>
                                <!--/ options -->
                            </div>
                            <!--/ question options -->
                            </div>
                            <ul class="pager wizard">
                                <li class="previous first"><a href="#">First</a></li>
                                <li class="previous"><a href="#">Previous</a></li>
                                <li class="next last"><a href="#">Submit Test</a></li>
                                <li class="next">
                                    <a href="#">Next</a>
                                    <a href="#">Skip Question</a>
                                </li>                                
                            </ul>
                        </div>
                    </div>
                    <!-- root wizard -->
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </main>
    <!--/ main -->
    <?php include 'includes/scripts.php'?> 
   
</body>
</html>